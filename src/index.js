import React from "react";
import reactDome from "react-dom";

import App from "./App"

reactDome.render(<App/>, document.getElementById('root'));