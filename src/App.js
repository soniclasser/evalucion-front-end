import React from "react";
import { BrowserRouter as Router, Route, Routes, } from "react-router-dom";
import { ExampleOne } from "./ExampleOne";
import { ExampleOneA } from './ExampleOneA';
import { ExampleTwo } from "./ExampleTwo";
import './style.css'
import { Home } from "./Home";

export default function App() {
    return ( < Router >
            <
            Routes >
            <
            Route path = "/"
            element = { < Home / > }
            />   < /
            Routes > <
            Routes >
            <
            Route path = "/example-1"
            element = { < ExampleOne totalDivs = { 100 }
                /> } / >
                <
                /Routes>   <
                Routes >
                <
                Route path = "/example-1-a"
                element = { < ExampleOneA / > }
                />   < /
                Routes > <
                Routes >
                <
                Route path = "/example-1-b"
                element = { < ExampleOneA validateOnlyNumber = { true }
                    />} / >
                    <
                    /Routes>   <
                    Routes >
                    <
                    Route path = "/example-2"
                    element = { < ExampleTwo / > }
                    />   < /
                    Routes > <
                    Routes >
                    <
                    Route path = "/example-2-b"
                    element = { < ExampleTwo mod = { 3 }
                        styleButton = { true }
                        />} / >
                        <
                        /Routes>  <
                        Routes >
                        <
                        Route path = "/example-3"
                        element = { < ExampleTwo ExampleTwo mod = { 3 }
                            styleButton = { true }
                            inLocal = { false }
                            /> } / >
                            <
                            /Routes>  <
                            Routes >
                            <
                            Route path = "/example-2-a"
                            element = { < ExampleTwo ExampleTwoinLocal = { false }
                                mod = { 3 }
                                /> } / >
                                <
                                /Routes>  < /
                                Router >
                            );
                        }