import React from "react";

export function ExampleOne(props) {
    const { totalDivs } = props
    const divDraw = totalDivs === 0 ? [] : Array.from(Array(totalDivs).keys());
    const updatePosition = (index) => index + 1;
    
    return (
        <div>
            {divDraw.map((item, i) => <div className={`color-${updatePosition(i) % 5}`} key={updatePosition(i)}>{updatePosition(i)}</div>)}
        </div>
    );
}