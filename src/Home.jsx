import React from "react";
import { Link } from 'react-router-dom'


export function Home() {

    return (
        <div>
            <h1>hola a todos</h1>
            <ul>
                <li>
                    <Link to="/example-1">Ejercicio 1</Link>
                </li>
                <li> 
                    <Link to="/example-1-a">Ejercicio 1 variante 1</Link>
                </li>
                <li>
                    <Link to="/example-1-b">Ejercicio 1 variante 2</Link>
                </li>
                <li>
                    <Link to="/example-2">Ejercicio 2</Link>
                </li>
                <li>
                    <Link to="/example-2-a">Ejercicio 2 variante 1</Link>
                </li>
                <li>
                    <Link to="/example-2-b">Ejercicio 2 variante 2</Link>
                </li>
                <li>
                    <Link to="/example-3">Ejercicio 3</Link>
                </li>
            </ul>
        </div>


    );
}