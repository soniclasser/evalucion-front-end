import React, { useState, useEffect } from "react";
import "./style.css"


export function ExampleTwo(props) {

    const { inLocal = true, mod, styleButton = false } = props
    const [counter, setCounter] = useState(0);
    const [id, setId] = useState('');


    useEffect(() => {
        if (!inLocal) {
            fetch("https://buxfua2s9j.execute-api.us-west-2.amazonaws.com/dev/")
                .then((response) => response.json())
                .then((data) => {
                    setCounter(data.items.length > 0 ? data.items[0].number_value : 0)
                    setId(data.items.length > 0 ? data.items[0].id : '')
                });
        }
    }, [id, inLocal]);

    const getOperation = async (operation) => {
        await fetch(`https://buxfua2s9j.execute-api.us-west-2.amazonaws.com/dev/${operation}`,
            {
                method: 'POST',
                body: JSON.stringify({number_value: counter}), 
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        ).then((response) => response.json())
        .then((data) => {
            upsert(data.data.value);
        });
    }

    const upsert = (value) => {
        fetch(`https://buxfua2s9j.execute-api.us-west-2.amazonaws.com/dev/`,
            {
                method: 'POST',
                body: JSON.stringify({
                    number_value: value,
                    id
                }), 
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        ).then((response) => response.json())
        .then((data) => {
            console.log(data)
            setCounter(data.is)
            setId(data.number_value)
        });
    }

    const add = () => {
        if (inLocal) {
            setCounter(counter + 1)
        } else {
            getOperation('add');
        }
    }

    const subtract = () => {
        if (inLocal) {
            setCounter(counter - 2)
        } else {
            getOperation('subtract');

        }
    }

    const divide = () => {
        if (inLocal) {
            setCounter(counter / 3)
        } else {
            getOperation('divide');
        } 
    }

    const reset = () => {
        if (inLocal) {
            setCounter(0)
        } else {
            getOperation('reset');
        } 
    }

    return (
        <div className="content-buttons">
            <button className={styleButton ? 'button-action' : ''} onClick={add}>Add</button>
            <button className={styleButton ? 'button-action' : ''} onClick={subtract}>Subtract</button>
            <button className={styleButton ? 'button-action' : ''} onClick={divide}>Divide</button>
            <button className={styleButton ? 'button-action' : ''} onClick={reset}>Reset</button>
            <div className="content-result">
                <span className={mod ? `result-${Math.abs(counter % mod)}` : 'result'}>{counter}</span>
            </div>
        </div>
    )
}