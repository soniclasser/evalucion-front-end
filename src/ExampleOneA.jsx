import React, { useState } from "react";
import { ExampleOne } from "./ExampleOne";

export function ExampleOneA(prop) {
    const { validateOnlyNumber = false } = prop
    const [textInput, setTextInput] = useState(0);

    const handleChange = (event) => {
        let value = Number(event.target.value)
        value = isNaN(value) ? 0 : value;
        setTextInput(value);
    }

    return (
        <div>
            <input onChange={handleChange} placeholder="ingresa un numero" onKeyPress={(event) => {
                if (validateOnlyNumber && !/[0-9]/.test(event.key)) {
                    event.preventDefault();
                }
            }} />
            <ExampleOne totalDivs={parseInt(textInput)} />
        </div>
    );
}